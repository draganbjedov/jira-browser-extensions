# README #

This is JIRA web extension for Firefox and Chrome that calculates remaining estimate on search pages. 

### How to use it ###
**Note:** Remaining estimate column must be shown.
Click on JIRA icon in toolbar and popup dialog will show remaining estimate.

### How to build ###
First clone repository.
```
git clone https://draganbjedov@bitbucket.org/draganbjedov/jira-browser-extensions.git
```
####Firefox
```
cd sum_firefox

#Pack extension
web-ext sign --api-key=$AMO_JWT_ISSUER --api-secret=$AMO_JWT_SECRET
```
The API options are required to specify your addons.mozilla.org credentials.

- --api-key: the API key (JWT issuer) from addons.mozilla.org needed to sign the extension. This should always be a string.
- --api-secret: the API secret (JWT secret) from addons.mozilla.org needed to sign the extension. This should always be a string.

####Chrome
```
cd sum_chrome

# Create your own pem file using following command:
openssl genrsa -des3 -out key.pem 2048

#Pack extension
./crxmake.sh . ./key.pem <version>
```

for key from repo pass phrase is: ```dragan```
