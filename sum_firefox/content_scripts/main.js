/* global browser */

function showSumOfRemaining() {
    var x = document.getElementsByClassName("timeestimate");
    var days = 0;
    var hours = 0;
    var minutes = 0;
	var count = 0;
    for (i = 0; i < x.length; i++) {
        var y = x[i].innerHTML.split(" ");
        if (y[0] !== "") {
			for (j = 0; j < y.length; j += 2) {
				if (y[j + 1].indexOf("week") !== -1)
					days += parseInt(y[j]) * 5;
				else if (y[j + 1].indexOf("day") !== -1)
					days += parseInt(y[j]);
				else if (y[j + 1].indexOf("hour") !== -1)
					hours += parseInt(y[j]);
				else if (y[j + 1].indexOf("minute") !== -1)
					minutes += parseInt(y[j]);
			}
			count++;
        }
    }
    hours += parseInt(minutes / 60);
    minutes = minutes % 60;
    days += parseInt(hours / 8);
    hours = hours % 8;
    console.log(days + "d " + hours + "h " + minutes + "m");
    var sumDays = (days + hours/8.0 + minutes/60.0/8.0).toFixed(2);
    alert(
		  "Information\n"
		+ "▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬\n"
		+ "Number of issues: " + count + "\n"
		+ "Remaining estimate sum: " + days + "d " + hours + "h " + minutes + "m" + "\n"
        + "Remaining estimate sum in days: " + sumDays);
    browser.runtime.onMessage.removeListener(showSumOfRemaining);
}

showSumOfRemaining();
