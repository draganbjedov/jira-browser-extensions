/* global browser */

function showSumOfRemaining() {
    browser.tabs.executeScript(null, {
        file: "/content_scripts/main.js"
    });
}

browser.browserAction.onClicked.addListener(showSumOfRemaining);